#pragma once
#include "Tensor.h"
#include <vector>
#include <fstream>
#include <filesystem>

template<typename T, size_t D>
struct Field 
{
    char* ptr;                  //pointer to the data
    Tensor<size_t,D> dims;      //in elements
    Tensor<size_t,D> strides;   //in bytes

    constexpr T& operator[](const Tensor<size_t,D>& indices);
    constexpr const T& operator[](const Tensor<size_t,D>& indices) const;
    constexpr Field<T,D> slice(const Tensor<size_t, D>& min, const Tensor<size_t, D>& max); //max is excluded

    constexpr std::vector<T> extract() const;
    constexpr std::ofstream open_file(std::string filename);
    constexpr void save(std::string filename);
};


template<typename T, size_t D>
constexpr T& Field<T,D>::operator[](const Tensor<size_t,D>& indices)
{
    return *((T*)(ptr + (indices*strides).sum()));
}

template<typename T, size_t D>
constexpr const T& Field<T,D>::operator[](const Tensor<size_t,D>& indices) const
{
    return *((T*)(ptr + (indices*strides).sum()));
}

template<typename T, size_t D>
constexpr Field<T,D> Field<T,D>::slice(const Tensor<size_t, D>& min, const Tensor<size_t, D>& max)
{
    Field<T,D> out = *this;
    for(size_t i=0; i<D; i++)
    {
        out.ptr += min[i] * this->strides[i];
        out.dims[i] = max[i] - min[i];
    }
    return out;
}

template<typename T, size_t D>
constexpr std::vector<T> Field<T,D>::extract() const
{
    std::vector<T> out(dims.product());

    Tensor<size_t,D> min{};
    Tensor<size_t,D> max{};
    for(size_t i=0; i<D; i++)
    {
        min[i] = 0;
        max[i] = dims[i] - 1;
    }
    Tensor<size_t,D> current = min;
    size_t i = 0;
    for(bool looping=true; looping; looping=iterate(min, max, current))
    {
        out[i] = (*this)[current];
        i++;
    }
    return out;
}




template<typename T, size_t D>
constexpr std::ofstream Field<T,D>::open_file(std::string filename)
{
    size_t last_delim = filename.find_last_of("/\\");
    if(last_delim != std::string::npos)
    {
        std::string path = filename.substr(0, last_delim);
        std::filesystem::create_directories(path);
    }
    return std::ofstream(filename, std::fstream::binary | std::fstream::app);
}

template<typename T, size_t D>
constexpr void Field<T,D>::save(std::string filename)
{
    std::ofstream file = open_file(filename);

    Tensor<size_t,D> min{};
    Tensor<size_t,D> max{};
    for(size_t i=0; i<D; i++)
    {
        min[i] = 0;
        max[i] = dims[i] - 1;
    }
    Tensor<size_t,D> current = min;
    for(bool looping=true; looping; looping=iterate(min, max, current))
    {
        file.write((char*)&((*this)[current]), sizeof(T));
    }

    file.close();
}

