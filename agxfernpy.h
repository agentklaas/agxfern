#pragma once

#include <agxSDK/Simulation.h>
#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <pybind11/stl.h>
#include <array>
#include "CGListener.h"
#include "DSCListener.h"

namespace py = pybind11;




////////////////////////////////////////////////////////////////////////////////////////////////
//function declarations and class definitions
////////////////////////////////////////////////////////////////////////////////////////////////

template<typename T, size_t D>
constexpr py::array_t<T> field_to_numpy(Field<T,D>& field);

template<typename T, size_t... D>
constexpr py::array_t<T> tensor_to_array(Tensor<T,D...>& tensor);


template<typename T>
struct py_Grid
{
    Grid<T,3> grid;

    py_Grid(Grid<T,3> inGrid);
    py_Grid(std::array<T,3> inMin, std::array<T,3> inMax, std::array<size_t,3> inCells);
    std::array<T,3> py_get_min();
    std::array<T,3> py_get_max();
    std::array<size_t,3> py_get_cells();
    std::array<T,3> py_cell_size();
    std::array<size_t,3> py_cell_at(std::array<T,3> in);
    std::array<size_t,3> py_bounded_cell_at(std::array<T,3> in);
    std::array<T,3> py_cell_center(std::array<size_t,3> in);

    T py_cell_volume();
    size_t py_size();
};


template<typename T>
struct py_DSCListener
{
    DSCListener<T>* lis;

    py_DSCListener(uint64_t inSystem, py_Grid<T> inGrid);
    void add_to_sim(uint64_t sim);
    py_Grid<T> get_grid();

    py::array_t<T> get_mass();
    py::array_t<T> get_momentum();
    py::array_t<T> get_kinetic_stress();
    py::array_t<T> get_local_force();
    py::array_t<T> get_force();
};


template<typename T>
struct py_CGListener
{
    CGListener<T>* lis;

    py_CGListener(uint64_t inSystem, py_Grid<T> inGrid, T cgwidth);
    void add_to_sim(uint64_t sim);
    py_Grid<T> get_grid();

    py::array_t<T> get_mass();
    py::array_t<T> get_momentum();
    py::array_t<T> get_kinetic_stress();
    py::array_t<T> get_local_force();
    py::array_t<T> get_force();
    py::array_t<T> get_contact_stress();
    py::array_t<T> get_velocity();
    py::array_t<T> get_displacement();
    py::array_t<T> get_pressure();
    py::array_t<T> get_stress();
    py::array_t<T> get_stress_deviator();
    py::array_t<T> get_stress_von_mises();
    py::array_t<T> get_strain();
    py::array_t<T> get_strain_rate();
};





////////////////////////////////////////////////////////////////////////////////////////////////
//function definitions
////////////////////////////////////////////////////////////////////////////////////////////////

template<typename T, size_t D>
constexpr py::array_t<T> field_to_numpy(const Field<T,D>& field)
{
    std::vector<py::ssize_t> dims(D);
    std::vector<py::ssize_t> strides(D);
    for(size_t i=0; i<D; i++)
    {
        dims[i] = field.dims[i];
        strides[i] = field.strides[i];
    }
    py::buffer_info buffer{field.ptr, sizeof(T), py::format_descriptor<T>::format(), D, dims, strides};
    return py::array_t<T>(buffer);
}

template<typename T, size_t... D>
constexpr py::array_t<T> tensor_to_array(Tensor<T,D...>& tensor)
{
    if constexpr(Tensor<T,D...>::ndim() == 0)
    {
        py::buffer_info buffer{tensor.ptr(), sizeof(T), py::format_descriptor<T>::format(), 1, std::vector<py::ssize_t>{1}, std::vector<py::ssize_t>{sizeof(T)}};
        return py::array_t<T>(buffer);
    }
    else if constexpr(Tensor<T,D...>::ndim() == 1)
    {
        py::buffer_info buffer{tensor.ptr(), sizeof(T), py::format_descriptor<T>::format(), Tensor<T,D...>::ndim(), std::vector<py::ssize_t>{D...}, std::vector<py::ssize_t>{constants::stride<D...,0>*sizeof(T)}};
        return py::array_t<T>(buffer);
    }
    else if constexpr(Tensor<T,D...>::ndim() == 2)
    {
        py::buffer_info buffer{tensor.ptr(), sizeof(T), py::format_descriptor<T>::format(), Tensor<T,D...>::ndim(), std::vector<py::ssize_t>{D...}, std::vector<py::ssize_t>{constants::stride<D...,0>*sizeof(T),constants::stride<D...,1>*sizeof(T)}};
        return py::array_t<T>(buffer);
    }
    else if constexpr(Tensor<T,D...>::ndim() == 3)
    {
        py::buffer_info buffer{tensor.ptr(), sizeof(T), py::format_descriptor<T>::format(), Tensor<T,D...>::ndim(), std::vector<py::ssize_t>{D...}, std::vector<py::ssize_t>{constants::stride<D...,0>*sizeof(T), constants::stride<D...,1>*sizeof(T), constants::stride<D...,2>*sizeof(T)}};
        return py::array_t<T>(buffer);
    }
    else
    {
        std::cout<<"No Conversion defined for Tensor of this Shape.";
        return py::array_t<T>();
    }
}



template<typename T>
inline py_Grid<T>::py_Grid(Grid<T,3> inGrid): grid(inGrid)
{

}

template<typename T>
inline py_Grid<T>::py_Grid(std::array<T,3> inMin, std::array<T,3> inMax, std::array<size_t,3> inCells): grid({inMin[0], inMin[1], inMin[2]}, {inMax[0], inMax[1], inMax[2]}, {inCells[0], inCells[1], inCells[2]})
{
    
}

template<typename T>
inline std::array<T,3> py_Grid<T>::py_get_min()
{
    return std::array<T,3>{grid.min[0], grid.min[1], grid.min[2]};
}

template<typename T>
inline std::array<T,3> py_Grid<T>::py_get_max()
{
    return std::array<T,3>{grid.max[0], grid.max[1], grid.max[2]};
}

template<typename T>
inline std::array<size_t,3> py_Grid<T>::py_get_cells()
{
    return std::array<size_t,3>{grid.cells[0], grid.cells[1], grid.cells[2]};
}

template<typename T>
inline std::array<T,3> py_Grid<T>::py_cell_size()
{
    return std::array<T,3>{grid.cell_size[0], grid.cell_size[1], grid.cell_size[2]};
}

template<typename T>
inline std::array<size_t,3> py_Grid<T>::py_cell_at(std::array<T,3> in)
{
    Tensor<size_t,3> cell = grid.cell_at(Tensor<T,3>{in[0], in[1], in[2]});
    return std::array<size_t,3>{cell[0],cell[1],cell[2]};
}

template<typename T>
inline std::array<size_t,3> py_Grid<T>::py_bounded_cell_at(std::array<T,3> in)
{
    Tensor<size_t,3> cell = grid.bounded_cell_at(Tensor<T,3>{in[0], in[1], in[2]});
    return std::array<size_t,3>{cell[0],cell[1],cell[2]};
}

template<typename T>
inline std::array<T,3> py_Grid<T>::py_cell_center(std::array<size_t,3> in)
{
    Tensor<T,3> center = grid.cell_center(Tensor<size_t,3>{in[0], in[1], in[2]});
    return std::array<T,3>{center[0], center[1], center[2]};
}

template<typename T>
inline T py_Grid<T>::py_cell_volume()
{
    return grid.cell_volume;
}

template<typename T>
inline size_t py_Grid<T>::py_size()
{
    return grid.size();       
}




template<typename T>
inline py_DSCListener<T>::py_DSCListener(uint64_t inSystem, py_Grid<T> inGrid)
{
    //put on Heap to prevent Python from garbage collecting it
    lis = new DSCListener<T>((agx::ParticleSystem*)inSystem, inGrid.grid);
}

template<typename T>
inline void py_DSCListener<T>::add_to_sim(uint64_t sim)
{
    agxSDK::Simulation* sp = (agxSDK::Simulation*)sim;
    sp->add(lis);
}

template<typename T>
inline py_Grid<T> py_DSCListener<T>::get_grid()
{
    return py_Grid<T>(lis->grid);
}


template<typename T>
inline py::array_t<T> py_DSCListener<T>::get_mass()
{
    return field_to_numpy(lis->get_mass());
}

template<typename T>
inline py::array_t<T> py_DSCListener<T>::get_momentum()
{
    return field_to_numpy(lis->get_momentum());
}

template<typename T>
inline py::array_t<T> py_DSCListener<T>::get_kinetic_stress()
{
    return field_to_numpy(lis->get_kinetic_stress());
}

template<typename T>
inline py::array_t<T> py_DSCListener<T>::get_local_force()
{
    return field_to_numpy(lis->get_local_force());
}

template<typename T>
inline py::array_t<T> py_DSCListener<T>::get_force()
{
    return field_to_numpy(lis->get_force());
}




template<typename T>
inline py_CGListener<T>::py_CGListener(uint64_t inSystem, py_Grid<T> inGrid, T cgwidth)
{
    lis = new CGListener<T>((agx::ParticleSystem*)inSystem, inGrid.grid, cgwidth);
}

template<typename T>
inline void py_CGListener<T>::add_to_sim(uint64_t sim)
{
    agxSDK::Simulation* sp = (agxSDK::Simulation*)sim;
    sp->add(lis);
}

template<typename T>
inline py_Grid<T> py_CGListener<T>::get_grid()
{
    return py_Grid<T>(lis->grid);
}



template<typename T>
inline py::array_t<T> py_CGListener<T>::get_mass()
{
    return field_to_numpy(lis->get_mass());
}

template<typename T>
inline py::array_t<T> py_CGListener<T>::get_momentum()
{
    return field_to_numpy(lis->get_momentum());
}

template<typename T>
inline py::array_t<T> py_CGListener<T>::get_kinetic_stress()
{
    return field_to_numpy(lis->get_kinetic_stress());
}

template<typename T>
inline py::array_t<T> py_CGListener<T>::get_local_force()
{
    return field_to_numpy(lis->get_local_force());
}

template<typename T>
inline py::array_t<T> py_CGListener<T>::get_force()
{
    return field_to_numpy(lis->get_force());
}

template<typename T>
inline py::array_t<T> py_CGListener<T>::get_contact_stress()
{
    return field_to_numpy(lis->get_contact_stress());
}

template<typename T>
inline py::array_t<T> py_CGListener<T>::get_velocity()
{
    return field_to_numpy(lis->get_velocity());
}

template<typename T>
inline py::array_t<T> py_CGListener<T>::get_displacement()
{
    return field_to_numpy(lis->get_displacement());
}

template<typename T>
inline py::array_t<T> py_CGListener<T>::get_pressure()
{
    return field_to_numpy(lis->get_pressure());
}

template<typename T>
inline py::array_t<T> py_CGListener<T>::get_stress()
{
    return field_to_numpy(lis->get_stress());
}

template<typename T>
inline py::array_t<T> py_CGListener<T>::get_stress_deviator()
{
    return field_to_numpy(lis->get_stress_deviator());
}

template<typename T>
inline py::array_t<T> py_CGListener<T>::get_stress_von_mises()
{
    return field_to_numpy(lis->get_stress_von_mises());
}

template<typename T>
inline py::array_t<T> py_CGListener<T>::get_strain()
{
    return field_to_numpy(lis->get_strain());
}

template<typename T>
inline py::array_t<T> py_CGListener<T>::get_strain_rate()
{
    return field_to_numpy(lis->get_strain_rate());
}
