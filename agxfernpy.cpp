#include "agxfernpy.h"

//We can't use templates in python, so we just build the whole module for a certain precision type
using T = float;


PYBIND11_MODULE(agxfernpy, m)
{
    m.doc() = "Python Bindings for the agx Coarse Graining Listener using the Fern Library.";


    py::class_<py_Grid<T>>(m, "Grid", "A struct that defines a coarse graining grid.")
    .def(py::init<std::array<T,3>,std::array<T,3>,std::array<size_t,3>>(), py::arg("mins"), py::arg("maxs"), py::arg("cells"))
    .def("get_min", &py_Grid<T>::py_get_min)
    .def("get_max", &py_Grid<T>::py_get_max)
    .def("get_cells", &py_Grid<T>::py_get_cells)
    .def("cell_size", &py_Grid<T>::py_cell_size)
    .def("cell_volume", &py_Grid<T>::py_cell_volume)
    .def("cell_at", &py_Grid<T>::py_cell_at)
    .def("bounded_cell_at", &py_Grid<T>::py_bounded_cell_at)
    .def("cell_center", &py_Grid<T>::py_cell_center)
    .def("size", &py_Grid<T>::py_size);


    py::class_<py_DSCListener<T>>(m, "DSCListener", "An AGX StepEventListener that calculates fields from DEM data.")
    .def(py::init<uint64_t, py_Grid<T>>(),py::arg("particleSystem"), py::arg("Grid"))
    .def("add_to_sim",&py_DSCListener<T>::add_to_sim)
    .def("get_grid",&py_DSCListener<T>::get_grid)
    .def("get_mass", &py_DSCListener<T>::get_mass)
    .def("get_momentum", &py_DSCListener<T>::get_momentum)
    .def("get_kinetic_stress", &py_DSCListener<T>::get_kinetic_stress)
    .def("get_local_force", &py_DSCListener<T>::get_local_force)
    .def("get_force", &py_DSCListener<T>::get_force);


    py::class_<py_CGListener<T>>(m, "CGListener", "An AGX StepEventListener that calculates fields from DEM data using Coarse Graining.")
    .def(py::init<uint64_t, py_Grid<T>, T>(),py::arg("particleSystem"), py::arg("Grid"), py::arg("cgwidth"))
    .def("add_to_sim",&py_CGListener<T>::add_to_sim)
    .def("get_grid",&py_CGListener<T>::get_grid)
    .def("get_mass", &py_CGListener<T>::get_mass)
    .def("get_momentum", &py_CGListener<T>::get_momentum)
    .def("get_kinetic_stress", &py_CGListener<T>::get_kinetic_stress)
    .def("get_local_force", &py_CGListener<T>::get_local_force)
    .def("get_force", &py_CGListener<T>::get_force)
    .def("get_contact_stress", &py_CGListener<T>::get_contact_stress)
    .def("get_velocity", &py_CGListener<T>::get_velocity)
    .def("get_displacement", &py_CGListener<T>::get_displacement)
    .def("get_pressure", &py_CGListener<T>::get_pressure)
    .def("get_stress", &py_CGListener<T>::get_stress)
    .def("get_stress_deviator", &py_CGListener<T>::get_stress_deviator)
    .def("get_stress_von_mises", &py_CGListener<T>::get_stress_von_mises)
    .def("get_strain", &py_CGListener<T>::get_strain)
    .def("get_strain_rate", &py_CGListener<T>::get_strain_rate);

}
