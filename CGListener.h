#pragma once

#include "Discretization.h"
#include "Kernels.h"
#include "Particle.h"
#include "Contact.h"
#include "Field.h"

#include <agx/RigidParticleSystem.h>
#include <agxSDK/StepEventListener.h>



using namespace Fern;

template<typename T = float>
struct CGListener: public agxSDK::StepEventListener
{
    //using PARTICLE_PROPS = Tuple<NE<Tensor<T>,"mass">, NE<Tensor<T,3>,"momentum">, NE<Tensor<T,3,3>,"stress">, NE<Tensor<T,3>,"mass_displacement">>;
    //using CONTACT_PROPS = Tuple<NE<Tensor<T,3>,"local_force">, NE<Tensor<T,3>,"force">, NE<Tensor<T,3,3>,"stress"> >;
    //using GRADIENT_PROPS = Tuple<NE<Tensor<T,3>,"momentum">, NE<Tensor<T,3>,"mass_displacement">>;
    //using GRADIENT_PROPS_D = typename Fern::Utils::TupleAddDims<GRADIENT_PROPS,3>;

    //using SECONDARY_PROPS = Tuple<NE<Tensor<T,3>,"velocity">, NE<Tensor<T,3>,"displacement">, NE<Tensor<T>,"pressure">, NE<Tensor<T,3,3>,"stress">, NE<Tensor<T,3,3>,"stress_deviator">, NE<Tensor<T>,"stress_von_mises">, NE<Tensor<T,3,3>,"strain">, NE<Tensor<T,3,3>,"strain_rate">>;

    using PARTICLE_PROPS = Tuple<Tensor<T>, Tensor<T,3>, Tensor<T,3,3>, Tensor<T,3>>;
    using CONTACT_PROPS = Tuple<Tensor<T,3>, Tensor<T,3>, Tensor<T,3,3> >;
    using GRADIENT_PROPS = Tuple<Tensor<T,3>, Tensor<T,3> >;
    using GRADIENT_PROPS_D = typename Fern::Utils::TupleAddDims<GRADIENT_PROPS,3>;
    using SECONDARY_PROPS = Tuple<Tensor<T,3>, Tensor<T,3>, Tensor<T>, Tensor<T,3,3>, Tensor<T,3,3>, Tensor<T>, Tensor<T,3,3>, Tensor<T,3,3> >;

    struct CGParticle
    {
        Tensor<T,3> position;
        PARTICLE_PROPS properties;
    };

    struct CGContact 
    {
        Tensor<T,3> position;
        CONTACT_PROPS properties;
    };

    struct CGGradientParticle
    {
        Tensor<T,3> position;
        GRADIENT_PROPS properties;
    };


    agx::ParticleSystem* system;
    Grid<T,3> grid;
    T cgwidth;

    std::vector<Tensor<T,3>> ref_positions;

    std::vector<PARTICLE_PROPS> particle_field;
    std::vector<CONTACT_PROPS> contact_field;
    std::vector<GRADIENT_PROPS_D> gradient_field;
    std::vector<SECONDARY_PROPS> secondary_field;


    constexpr CGListener(agx::ParticleSystem* inSystem, const Fern::Grid<T,3>& inGrid, const T& inCgwidth);
    constexpr void post(const agx::TimeStamp& time) override;

    constexpr void reference_particles();
    constexpr std::vector<CGParticle> retrieve_particles() const;
    constexpr std::vector<CGContact> retrieve_contacts() const;
    constexpr std::vector<CGGradientParticle> retrieve_gradient_particles() const;

    constexpr void calc_secondary();


    constexpr Field<T,3> get_mass();
    constexpr Field<T,4> get_momentum();
    constexpr Field<T,5> get_kinetic_stress();
    constexpr Field<T,4> get_local_force();
    constexpr Field<T,4> get_force();
    constexpr Field<T,5> get_contact_stress();
    constexpr Field<T,4> get_velocity();
    constexpr Field<T,4> get_displacement();
    constexpr Field<T,3> get_pressure();
    constexpr Field<T,5> get_stress();
    constexpr Field<T,5> get_stress_deviator();
    constexpr Field<T,3> get_stress_von_mises();
    constexpr Field<T,5> get_strain();
    constexpr Field<T,5> get_strain_rate();
};



template<typename T>
constexpr CGListener<T>::CGListener(agx::ParticleSystem* inSystem, const Fern::Grid<T,3>& inGrid, const T& inCgwidth): system(inSystem), grid(inGrid), cgwidth(inCgwidth)
{

}

template<typename T>
constexpr void CGListener<T>::post(const agx::TimeStamp& time)
{
    if(!ref_positions.size())
    {
        reference_particles();
    }
    
    particle_field = CG<Kernels::Gaussian>(retrieve_particles(), grid, cgwidth);
    contact_field = CG<Kernels::Heaviside>(retrieve_contacts(), grid, cgwidth);
    gradient_field = CGGradient<Kernels::Gaussian>(retrieve_gradient_particles(), grid, cgwidth);
    calc_secondary();
}


template<typename T>
constexpr void CGListener<T>::reference_particles()
{
    ref_positions.resize(system->getNumParticles());
    for(size_t i=0; i<ref_positions.size(); i++)
    {
        Particle<T> tmp(system->getParticle(i));
        ref_positions[i] = tmp.position;
    }
}

template<typename T>
constexpr std::vector<typename CGListener<T>::CGParticle> CGListener<T>::retrieve_particles() const
{
    std::vector<CGParticle> particles(system->getNumParticles());
    for(size_t i=0; i<particles.size(); i++)
    {
        CGParticle& particle = particles[i]; 

        Particle<T> tmp(system->getParticle(i));
        Tensor<T,3> momentum = tmp.mass * tmp.velocity;

        particle.position = tmp.position;
        particle.properties.template get<0>()[0] = tmp.mass;
        particle.properties.template get<1>() = momentum;
        particle.properties.template get<2>() = outer_product(-momentum, tmp.velocity);
        particle.properties.template get<3>() = tmp.mass * (tmp.position-ref_positions[i]);
    }
    return particles;
}

template<typename T>
constexpr std::vector<typename CGListener<T>::CGContact> CGListener<T>::retrieve_contacts() const
{
    agx::Vector<agx::Physics::ParticlePairContactPtr> agx_contacts = system->getParticleParticleContacts(); 
    std::vector<CGContact> contacts(agx_contacts.size());
    for(size_t i=0; i<contacts.size(); i++)
    {
        CGContact& contact = contacts[i]; 
        Contact<T> tmp(agx_contacts[i], system->getParticle(agx_contacts[i].particleId1()), system->getParticle(agx_contacts[i].particleId2()));

        contact.position = tmp.point;
        contact.properties.template get<0>() = tmp.local_force;
        contact.properties.template get<1>() = tmp.local_force[0]*tmp.normal + tmp.local_force[1]*tmp.tangent_u + tmp.local_force[2]*tmp.tangent_v;
        contact.properties.template get<2>() = outer_product(-contact.properties.template get<1>(), tmp.branch);
    }
    return contacts;
}

template<typename T>
constexpr std::vector<typename CGListener<T>::CGGradientParticle> CGListener<T>::retrieve_gradient_particles() const
{
    std::vector<CGGradientParticle> gradient_particles(system->getNumParticles());
    for(size_t i=0; i<gradient_particles.size(); i++)
    {
        CGGradientParticle& gradient_particle = gradient_particles[i]; 
        Particle<T> tmp(system->getParticle(i));

        gradient_particle.position = tmp.position;
        gradient_particle.properties.template get<0>() = tmp.mass * tmp.velocity;
        gradient_particle.properties.template get<1>() = tmp.mass * (tmp.position-ref_positions[i]);
    }
    return gradient_particles;
}

template<typename T>
constexpr void CGListener<T>::calc_secondary()
{
    size_t grid_size = grid.size();
    secondary_field.resize(grid_size);
    for(size_t i=0; i<grid_size; i++)
    {
        T reciproc_mass = T(1.0) / particle_field[i].template get<0>()[0];
        if(!std::isnormal(reciproc_mass))
        {
            reciproc_mass = 0.0;
        }

        secondary_field[i].template get<0>() = particle_field[i].template get<1>() * reciproc_mass;
        secondary_field[i].template get<1>() = particle_field[i].template get<3>() * reciproc_mass;


        T& pressure = secondary_field[i].template get<2>()[0];
        Tensor<T,3,3>& stress = secondary_field[i].template get<3>();
        Tensor<T,3,3>& stress_deviator = secondary_field[i].template get<4>();
        Tensor<T>& stress_von_mises = secondary_field[i].template get<5>();

        stress = particle_field[i].template get<2>() + contact_field[i].template get<2>();
        pressure = T(-1.0/3.0) * (stress[{0,0}] + stress[{1,1}] + stress[{2,2}]);
        stress_deviator = stress;
        stress_deviator[{0,0}] += pressure;
        stress_deviator[{1,1}] += pressure;
        stress_deviator[{2,2}] += pressure;
        stress_von_mises = {std::sqrt(T(1.5)*stress_deviator.normsq())};
        


        Tensor<T,3,3> velocity_gradient = gradient_field[i].template get<0>() * reciproc_mass;
        Tensor<T,3,3> displacement_gradient = gradient_field[i].template get<1>() * reciproc_mass;

        Tensor<T,3,3>& strain = secondary_field[i].template get<6>();
        Tensor<T,3,3>& strain_rate = secondary_field[i].template get<7>();
        for(size_t j=0; j<3; j++)
        {
            for(size_t k=0; k<3; k++)
            {
                strain[{j,k}] = 0.5 * (displacement_gradient[{j,k}] + displacement_gradient[{k,j}]);
                strain_rate[{j,k}] = 0.5 * (velocity_gradient[{j,k}] + velocity_gradient[{k,j}]);
            }
        }
    }
}


template<typename T>
constexpr Field<T,3> CGListener<T>::get_mass()
{
    char* ptr = (char*)particle_field[0].template get<0>().ptr();
    Tensor<size_t,3> dims = {grid.cells[0], grid.cells[1], grid.cells[2]};
    Tensor<size_t,3> strides{grid.cells[1]*grid.cells[2]*sizeof(PARTICLE_PROPS), grid.cells[2]*sizeof(PARTICLE_PROPS), sizeof(PARTICLE_PROPS)};
    return Field<T,3>{ptr, dims, strides};
}

template<typename T>
constexpr Field<T,4> CGListener<T>::get_momentum()
{
    char* ptr = (char*)particle_field[0].template get<1>().ptr();
    Tensor<size_t,4> dims = {grid.cells[0], grid.cells[1], grid.cells[2], 3};
    Tensor<size_t,4> strides{grid.cells[1]*grid.cells[2]*sizeof(PARTICLE_PROPS), grid.cells[2]*sizeof(PARTICLE_PROPS), sizeof(PARTICLE_PROPS), sizeof(T)};
    return Field<T,4>{ptr, dims, strides};
}

template<typename T>
constexpr Field<T,5> CGListener<T>::get_kinetic_stress()
{
    char* ptr = (char*)particle_field[0].template get<2>().ptr();
    Tensor<size_t,5> dims = {grid.cells[0], grid.cells[1], grid.cells[2], 3, 3};
    Tensor<size_t,5> strides{grid.cells[1]*grid.cells[2]*sizeof(PARTICLE_PROPS), grid.cells[2]*sizeof(PARTICLE_PROPS), sizeof(PARTICLE_PROPS), 3*sizeof(T), sizeof(T)};
    return Field<T,5>{ptr, dims, strides};
}

template<typename T>
constexpr Field<T,4> CGListener<T>::get_local_force()
{
    char* ptr = (char*)contact_field[0].template get<0>().ptr();
    Tensor<size_t,4> dims = {grid.cells[0], grid.cells[1], grid.cells[2], 3};
    Tensor<size_t,4> strides{grid.cells[1]*grid.cells[2]*sizeof(CONTACT_PROPS), grid.cells[2]*sizeof(CONTACT_PROPS), sizeof(CONTACT_PROPS), sizeof(T)};
    return Field<T,4>{ptr, dims, strides};
}

template<typename T>
constexpr Field<T,4> CGListener<T>::get_force()
{
    char* ptr = (char*)contact_field[0].template get<1>().ptr();
    Tensor<size_t,4> dims = {grid.cells[0], grid.cells[1], grid.cells[2], 3};
    Tensor<size_t,4> strides{grid.cells[1]*grid.cells[2]*sizeof(CONTACT_PROPS), grid.cells[2]*sizeof(CONTACT_PROPS), sizeof(CONTACT_PROPS), sizeof(T)};
    return Field<T,4>{ptr, dims, strides};
}

template<typename T>
constexpr Field<T,5> CGListener<T>::get_contact_stress()
{
    char* ptr = (char*)contact_field[0].template get<2>().ptr();
    Tensor<size_t,5> dims = {grid.cells[0], grid.cells[1], grid.cells[2], 3, 3};
    Tensor<size_t,5> strides{grid.cells[1]*grid.cells[2]*sizeof(CONTACT_PROPS), grid.cells[2]*sizeof(CONTACT_PROPS), sizeof(CONTACT_PROPS), 3*sizeof(T), sizeof(T)};
    return Field<T,5>{ptr, dims, strides};
}

template<typename T>
constexpr Field<T,4> CGListener<T>::get_velocity()
{
    char* ptr = (char*)secondary_field[0].template get<0>().ptr();
    Tensor<size_t,4> dims = {grid.cells[0], grid.cells[1], grid.cells[2], 3};
    Tensor<size_t,4> strides{grid.cells[1]*grid.cells[2]*sizeof(SECONDARY_PROPS), grid.cells[2]*sizeof(SECONDARY_PROPS), sizeof(SECONDARY_PROPS), sizeof(T)};
    return Field<T,4>{ptr, dims, strides};
}

template<typename T>
constexpr Field<T,4> CGListener<T>::get_displacement()
{
    char* ptr = (char*)secondary_field[0].template get<1>().ptr();
    Tensor<size_t,4> dims = {grid.cells[0], grid.cells[1], grid.cells[2], 3};
    Tensor<size_t,4> strides{grid.cells[1]*grid.cells[2]*sizeof(SECONDARY_PROPS), grid.cells[2]*sizeof(SECONDARY_PROPS), sizeof(SECONDARY_PROPS), sizeof(T)};
    return Field<T,4>{ptr, dims, strides};
}

template<typename T>
constexpr Field<T,3> CGListener<T>::get_pressure()
{
    char* ptr = (char*)secondary_field[0].template get<2>().ptr();
    Tensor<size_t,3> dims = {grid.cells[0], grid.cells[1], grid.cells[2]};
    Tensor<size_t,3> strides{grid.cells[1]*grid.cells[2]*sizeof(SECONDARY_PROPS), grid.cells[2]*sizeof(SECONDARY_PROPS), sizeof(SECONDARY_PROPS)};
    return Field<T,3>{ptr, dims, strides};
}

template<typename T>
constexpr Field<T,5> CGListener<T>::get_stress()
{
    char* ptr = (char*)secondary_field[0].template get<3>().ptr();
    Tensor<size_t,5> dims = {grid.cells[0], grid.cells[1], grid.cells[2], 3, 3};
    Tensor<size_t,5> strides{grid.cells[1]*grid.cells[2]*sizeof(SECONDARY_PROPS), grid.cells[2]*sizeof(SECONDARY_PROPS), sizeof(SECONDARY_PROPS), 3*sizeof(T), sizeof(T)};
    return Field<T,5>{ptr, dims, strides};
}

template<typename T>
constexpr Field<T,5> CGListener<T>::get_stress_deviator()
{
    char* ptr = (char*)secondary_field[0].template get<4>().ptr();
    Tensor<size_t,5> dims = {grid.cells[0], grid.cells[1], grid.cells[2], 3, 3};
    Tensor<size_t,5> strides{grid.cells[1]*grid.cells[2]*sizeof(SECONDARY_PROPS), grid.cells[2]*sizeof(SECONDARY_PROPS), sizeof(SECONDARY_PROPS), 3*sizeof(T), sizeof(T)};
    return Field<T,5>{ptr, dims, strides};
}

template<typename T>
constexpr Field<T,3> CGListener<T>::get_stress_von_mises()
{
    char* ptr = (char*)secondary_field[0].template get<5>().ptr();
    Tensor<size_t,3> dims = {grid.cells[0], grid.cells[1], grid.cells[2]};
    Tensor<size_t,3> strides{grid.cells[1]*grid.cells[2]*sizeof(SECONDARY_PROPS), grid.cells[2]*sizeof(SECONDARY_PROPS), sizeof(SECONDARY_PROPS)};
    return Field<T,3>{ptr, dims, strides};
}

template<typename T>
constexpr Field<T,5> CGListener<T>::get_strain()
{
    char* ptr = (char*)secondary_field[0].template get<6>().ptr();
    Tensor<size_t,5> dims = {grid.cells[0], grid.cells[1], grid.cells[2], 3, 3};
    Tensor<size_t,5> strides{grid.cells[1]*grid.cells[2]*sizeof(SECONDARY_PROPS), grid.cells[2]*sizeof(SECONDARY_PROPS), sizeof(SECONDARY_PROPS), 3*sizeof(T), sizeof(T)};
    return Field<T,5>{ptr, dims, strides};
}

template<typename T>
constexpr Field<T,5> CGListener<T>::get_strain_rate()
{
    char* ptr = (char*)secondary_field[0].template get<7>().ptr();
    Tensor<size_t,5> dims = {grid.cells[0], grid.cells[1], grid.cells[2], 3, 3};
    Tensor<size_t,5> strides{grid.cells[1]*grid.cells[2]*sizeof(SECONDARY_PROPS), grid.cells[2]*sizeof(SECONDARY_PROPS), sizeof(SECONDARY_PROPS), 3*sizeof(T), sizeof(T)};
    return Field<T,5>{ptr, dims, strides};
}
