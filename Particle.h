#pragma once

#include "Tensor.h"
#include <agx/RigidParticleSystem.h>


template<typename T>
struct Particle
{
    Tensor<T,3> position;
    Tensor<T,3> velocity;
    T radius;
    T mass;

    constexpr Particle(const agx::Physics::ParticlePtr& in);
};

template<typename T>
constexpr Particle<T>::Particle(const agx::Physics::ParticlePtr& in)
{
    position = {(T)in.position()[0], (T)in.position()[1], (T)in.position()[2]};
    radius = (T)in.radius();
    mass = {(T)in.mass()};
    velocity = {(T)in.velocity()[0], (T)in.velocity()[1], (T)in.velocity()[2]};
}
