#pragma once

#include "Tensor.h"
#include <agx/RigidParticleSystem.h>


template<typename T>
struct Contact
{
    Tensor<T,3> point;
    T depth;
    Tensor<T,3> velocity;

    Tensor<T,3> normal;
    Tensor<T,3> tangent_u;
    Tensor<T,3> tangent_v;
    Tensor<T,3> local_force;

    Tensor<T,3> branch;

    constexpr Contact(const agx::Physics::ParticlePairContactPtr& in, const agx::Physics::ParticlePtr& p0, const agx::Physics::ParticlePtr& p1);
};


template<typename T>
constexpr Contact<T>::Contact(const agx::Physics::ParticlePairContactPtr& in, const agx::Physics::ParticlePtr& p0, const agx::Physics::ParticlePtr& p1)
{
    point = {(T)in.point()[0], (T)in.point()[1], (T)in.point()[2]};
    depth = (T)in.depth();
    velocity = {(T)in.velocity()[0], (T)in.velocity()[1], (T)in.velocity()[2]};

    normal = {(T)in.normal()[0], (T)in.normal()[1], (T)in.normal()[2]};
    tangent_u = {(T)in.tangentU()[0], (T)in.tangentU()[1], (T)in.tangentU()[2]};
    tangent_v = {(T)in.tangentV()[0], (T)in.tangentV()[1], (T)in.tangentV()[2]};
    local_force = {(T)in.localForce()[0], (T)in.localForce()[1], (T)in.localForce()[2]};

    branch = {(T)(p1.position()[0]-p0.position()[0]), (T)(p1.position()[1]-p0.position()[1]), (T)(p1.position()[2]-p0.position()[2])};
}
