#pragma once

#include "Discretization.h"
#include "Particle.h"
#include "Contact.h"
#include "Field.h"

#include <agx/RigidParticleSystem.h>
#include <agxSDK/StepEventListener.h>



using namespace Fern;

template<typename T = float>
struct DSCListener: public agxSDK::StepEventListener
{
    //using PARTICLE_PROPS = Tuple<NE<Tensor<T>,"mass">, NE<Tensor<T,3>,"momentum">, NE<Tensor<T,3,3>,"stress"> >;
    //using CONTACT_PROPS = Tuple<NE<Tensor<T,3>,"local_force">, NE<Tensor<T,3>,"force"> >;
    using PARTICLE_PROPS = Tuple<Tensor<T>, Tensor<T,3>, Tensor<T,3,3>>;
    using CONTACT_PROPS = Tuple<Tensor<T,3>, Tensor<T,3> >;

    struct DSCParticle
    {
        Tensor<T,3> position;
        T radius;
        PARTICLE_PROPS properties;
    };

    struct DSCContact 
    {
        Tensor<T,3> position;
        T radius;
        CONTACT_PROPS properties;
    };


    agx::ParticleSystem* system;
    Grid<T,3> grid;

    std::vector<PARTICLE_PROPS> particle_field;
    std::vector<CONTACT_PROPS> contact_field;


    constexpr DSCListener(agx::ParticleSystem* inSystem, const Fern::Grid<T,3>& inGrid);
    constexpr void post(const agx::TimeStamp& time) override;

    constexpr std::vector<DSCParticle> retrieve_particles() const;
    constexpr std::vector<DSCContact> retrieve_contacts() const;

    constexpr Field<T,3> get_mass();
    constexpr Field<T,4> get_momentum();
    constexpr Field<T,5> get_kinetic_stress();
    constexpr Field<T,4> get_local_force();
    constexpr Field<T,4> get_force();
};



template<typename T>
constexpr DSCListener<T>::DSCListener(agx::ParticleSystem* inSystem, const Fern::Grid<T,3>& inGrid): system(inSystem), grid(inGrid)
{

}

template<typename T>
constexpr void DSCListener<T>::post(const agx::TimeStamp& time)
{
    particle_field = Discretize(retrieve_particles(), grid);
    contact_field = Discretize(retrieve_contacts(), grid);
}

template<typename T>
constexpr std::vector<typename DSCListener<T>::DSCParticle> DSCListener<T>::retrieve_particles() const
{
    std::vector<DSCParticle> particles(system->getNumParticles());
    for(size_t i=0; i<particles.size(); i++)
    {
        DSCParticle& particle = particles[i]; 

        Particle<T> tmp(system->getParticle(i));
        Tensor<T,3> momentum = tmp.mass * tmp.velocity;

        particle.position = tmp.position;
        particle.radius = tmp.radius;
        //particle.properties.template get<"mass">()[0] = tmp.mass;
        //particle.properties.template get<"momentum">() = momentum;
        //particle.properties.template get<"stress">() = outer_product(-momentum, tmp.velocity);
        particle.properties.template get<0>()[0] = tmp.mass;
        particle.properties.template get<1>() = momentum;
        particle.properties.template get<2>() = outer_product(-momentum, tmp.velocity);
    }
    return particles;
}

template<typename T>
constexpr std::vector<typename DSCListener<T>::DSCContact> DSCListener<T>::retrieve_contacts() const
{
    agx::Vector<agx::Physics::ParticlePairContactPtr> agx_contacts = system->getParticleParticleContacts(); 
    std::vector<DSCContact> contacts(agx_contacts.size());
    for(size_t i=0; i<contacts.size(); i++)
    {
        DSCContact& contact = contacts[i]; 
        Contact<T> tmp(agx_contacts[i], system->getParticle(agx_contacts[i].particleId1()), system->getParticle(agx_contacts[i].particleId2()));

        contact.position = tmp.point;
        contact.radius = tmp.depth;
        //contact.properties.template get<"local_force">() = tmp.local_force;
        //contact.properties.template get<"force">() = tmp.local_force[0]*tmp.normal + tmp.local_force[1]*tmp.tangent_u + tmp.local_force[2]*tmp.tangent_v;
        contact.properties.template get<0>() = tmp.local_force;
        contact.properties.template get<1>() = tmp.local_force[0]*tmp.normal + tmp.local_force[1]*tmp.tangent_u + tmp.local_force[2]*tmp.tangent_v;
    }
    return contacts;
}


template<typename T>
constexpr Field<T,3> DSCListener<T>::get_mass()
{
    char* ptr = (char*)particle_field[0].template get<0>().ptr();
    Tensor<size_t,3> dims = {grid.cells[0], grid.cells[1], grid.cells[2]};
    Tensor<size_t,3> strides{grid.cells[1]*grid.cells[2]*sizeof(PARTICLE_PROPS), grid.cells[2]*sizeof(PARTICLE_PROPS), sizeof(PARTICLE_PROPS)};
    return Field<T,3>{ptr, dims, strides};
}

template<typename T>
constexpr Field<T,4> DSCListener<T>::get_momentum()
{
    char* ptr = (char*)particle_field[0].template get<1>().ptr();
    Tensor<size_t,4> dims = {grid.cells[0], grid.cells[1], grid.cells[2], 3};
    Tensor<size_t,4> strides{grid.cells[1]*grid.cells[2]*sizeof(PARTICLE_PROPS), grid.cells[2]*sizeof(PARTICLE_PROPS), sizeof(PARTICLE_PROPS), sizeof(T)};
    return Field<T,4>{ptr, dims, strides};
}

template<typename T>
constexpr Field<T,5> DSCListener<T>::get_kinetic_stress()
{
    char* ptr = (char*)particle_field[0].template get<2>().ptr();
    Tensor<size_t,5> dims = {grid.cells[0], grid.cells[1], grid.cells[2], 3, 3};
    Tensor<size_t,5> strides{grid.cells[1]*grid.cells[2]*sizeof(PARTICLE_PROPS), grid.cells[2]*sizeof(PARTICLE_PROPS), sizeof(PARTICLE_PROPS), 3*sizeof(T), sizeof(T)};
    return Field<T,5>{ptr, dims, strides};
}

template<typename T>
constexpr Field<T,4> DSCListener<T>::get_local_force()
{
    char* ptr = (char*)contact_field[0].template get<0>().ptr();
    Tensor<size_t,4> dims = {grid.cells[0], grid.cells[1], grid.cells[2], 3};
    Tensor<size_t,4> strides{grid.cells[1]*grid.cells[2]*sizeof(CONTACT_PROPS), grid.cells[2]*sizeof(CONTACT_PROPS), sizeof(CONTACT_PROPS), sizeof(T)};
    return Field<T,4>{ptr, dims, strides};
}

template<typename T>
constexpr Field<T,4> DSCListener<T>::get_force()
{
    char* ptr = (char*)contact_field[0].template get<1>().ptr();
    Tensor<size_t,4> dims = {grid.cells[0], grid.cells[1], grid.cells[2], 3};
    Tensor<size_t,4> strides{grid.cells[1]*grid.cells[2]*sizeof(CONTACT_PROPS), grid.cells[2]*sizeof(CONTACT_PROPS), sizeof(CONTACT_PROPS), sizeof(T)};
    return Field<T,4>{ptr, dims, strides};
}
