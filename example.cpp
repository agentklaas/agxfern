#include "DSCListener.h"
#include "CGListener.h"

#include <agx/version.h>
#include <agx/RigidParticleSystem.h>
#include <agx/Physics/GranularBodySystem.h>
#include <agx/ParticleEmitter.h>
#include <agx/Prismatic.h>

#include <agxSDK/StepEventListener.h>
#include <agxSDK/Simulation.h>

#include <agxCollide/Box.h>
#include <agxCollide/Cylinder.h>
#include <agxCollide/Sphere.h>
#include <agxCollide/HeightField.h>
#include <agxCollide/Capsule.h>

#include <agxRender/RenderManager.h>

#include <agxOSG/ExampleApplication.h>
#include <agxOSG/utils.h>
#include <agxOSG/ReaderWriter.h>

#include <iomanip>
#include <filesystem>




///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//An exemplary class that shows how to access the coarse graining data.
//You will find how to add the coarse graining listener itself to your simulation further down.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename T, template<typename>typename LIS>
class FieldPrinter: public agxSDK::StepEventListener
{
    private:
    LIS<T>* cglistener;

    public:
    FieldPrinter(LIS<T>* in): cglistener(in)
    {

    }

    void post(const agx::TimeStamp& time) override
    {
        T field_mass = 0.0;
        Tensor<T,3> field_momentum{0.0,0.0,0.0};

        Field<T,3> mass = cglistener->get_mass();
        Field<T,4> momentum = cglistener->get_momentum();
        for(size_t i=0; i<cglistener->grid.cells[0]; i++)
        {
            for(size_t j=0; j<cglistener->grid.cells[1]; j++)
            {
                for(size_t k=0; k<cglistener->grid.cells[2]; k++)
                {
                    field_mass += mass[{i,j,k}];   
                    for(size_t l=0; l<3; l++)
                    {
                        field_momentum[l] += momentum[{i,j,k,l}];
                    }
                }
            }
        }
        field_mass *= cglistener->grid.cell_volume;
        field_momentum *= cglistener->grid.cell_volume;

        std::cout<<field_mass<<"\t";
        std::cout<<field_momentum<<"\n";
    }


};






///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Here starts the scene description etc.
//If you are familiar with agx, then you can ignore this part.
//It is transcribed from the python tutorial_granularBodies.py, that ships with agx.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class MotorStepListener: public agxSDK::StepEventListener
{
    private:
    agx::Motor1D* motor;
    agx::TimeStamp interval;
    agx::TimeStamp last;
    agx::Real speed;

    public:
    MotorStepListener(agx::Motor1D* inMotor, const agx::TimeStamp& inInterval, const agx::TimeStamp& inSpeed): motor(inMotor), interval(inInterval), last(0), speed(inSpeed)
    {

    }

    void pre(const agx::TimeStamp& time)
    {
        if(time-last >= interval)
        {
            last = time;
            speed = -1.0*speed;
            motor->setSpeed(speed);
        }
    }
};

void createAlternatingSpeedController(agxSDK::Simulation* sim, agx::Motor1D* motor, agx::Real speed, agx::TimeStamp interval)
{
    MotorStepListener* listener = new MotorStepListener(motor, interval, speed);
    motor->setEnable(true);
    motor->setSpeed(speed);
    sim->add(listener);
}

void setRenderColor(osg::Group* node, agx::Vec4f difColor, float shininess, float alpha)
{
    agxOSG::setDiffuseColor(node, difColor);
    agxOSG::setAmbientColor(node, agx::Vec4f(1.0, 1.0, 1.0, 0));
    agxOSG::setSpecularColor(node, agx::Vec4f(1.0, 1.0, 1.0, 1.0));
    agxOSG::setShininess(node, shininess);
    agxOSG::setAlpha(node, alpha);
}

inline osg::Group* buildScene1(agxSDK::Simulation* sim, agxOSG::ExampleApplication* app)
{
    osg::Group* root = new osg::Group;

    agx::setNumThreads(0);
    int n = agx::getNumThreads() / 2;
    agx::setNumThreads(n);
    sim->getDynamicsSystem()->getSolver()->setUseParallelPgs(true);
    sim->getDynamicsSystem()->getSolver()->setUse32bitGranularBodySolver(true);

    agxCollide::Geometry* floor_geometry = new agxCollide::Geometry(new agxCollide::Box(10.0, 10.0, 0.2));
    agx::RigidBody* floor = new agx::RigidBody(floor_geometry);
    floor->setPosition(0, 0, -0.2);
    floor->setMotionControl(agx::RigidBody::STATIC);
    sim->add(floor);
    agxOSG::createVisual(floor, root);

    agx::Physics::GranularBodySystem* granularBodySystem = new agx::Physics::GranularBodySystem();
    sim->add(granularBodySystem);

    agx::Material* pelletmat = new agx::Material("pellet");
    agx::Material* floormat = new agx::Material("floor");

    pelletmat->getBulkMaterial()->setDensity(2500);
    agx::ContactMaterial* c = new agx::ContactMaterial(pelletmat, floormat);
    agx::ContactMaterial* c2 = new agx::ContactMaterial(pelletmat, pelletmat);
    c->setYoungsModulus(1.1e+11);
    c->setFrictionCoefficient(0.5);
    c->setRollingResistanceCoefficient(0.3);

    c2->setYoungsModulus(1.1e+11);
    c2->setFrictionCoefficient(0.5);
    c2->setRollingResistanceCoefficient(0.3);

    sim->add(c);
    sim->add(c2);


    granularBodySystem->setParticleRadius(0.08);
    granularBodySystem->setMaterial(pelletmat);
    floor_geometry->setMaterial(floormat);

    agx::Bound3 containerBound(agx::Vec3(-1.0, -1.0, 3.0), agx::Vec3(1.0, 1.0, 6.0));
    granularBodySystem->spawnParticlesInBound(containerBound, granularBodySystem->getParticleRadius(), agx::Vec3(2.0*granularBodySystem->getParticleRadius()), 0.3*granularBodySystem->getParticleRadius());
    agx::Physics::GranularBodyPtrArray particles = granularBodySystem->getParticles();

    std::cout<<"Particle system now has "<<granularBodySystem->getNumParticles()<<" particles.\n";
    app->getSceneDecorator()->setText(1, "#particles: " + std::to_string(granularBodySystem->getNumParticles()));

    agxOSG::createVisual(granularBodySystem, root);

    agx::Vec3 size(2 * 0.8, 0.1, 3 * 0.8);
    agx::Vec3 pos = agx::Vec3(-1.0, -1.0, 4.0) + agx::Vec3(size.x()*0.6, size.y()*-1.9, size.z()*-0.2);
    double rotation = 20.0;

    agxSDK::Assembly* containerAssembly = new agxSDK::Assembly();

    agxCollide::Geometry* w1 = new agxCollide::Geometry(new agxCollide::Box(size));
    w1->setPosition(pos.x(), pos.y(), pos.z());
    w1->setRotation(agx::EulerAngles(rotation*2.0*3.14159265/360.0, 0.0, 0.0));
    containerAssembly->add(w1);

    agxCollide::Geometry* w2 = new agxCollide::Geometry(new agxCollide::Box(size));
    w2->setPosition(-pos.y(), pos.x(), pos.z());
    w2->setRotation(agx::EulerAngles(rotation*2.0*3.14159265/360.0, 0, 3.14159265/2.0));
    containerAssembly->add(w2);

    agxCollide::Geometry* w3 = new agxCollide::Geometry(new agxCollide::Box(size));
    w3->setPosition(pos.y(), -pos.x(), pos.z());
    w3->setRotation(agx::EulerAngles(rotation*2.0*3.14159265/360.0, 0, -3.14159265/2.0));
    containerAssembly->add(w3);

    agxCollide::Geometry* w4 = new agxCollide::Geometry(new agxCollide::Box(size));
    w4->setPosition(pos.x(), -pos.y(), pos.z());
    w4->setRotation(agx::EulerAngles(rotation*2.0*3.14159265/360.0, 0, 3.14159265));
    containerAssembly->add(w4);

    sim->add(containerAssembly);


    osg::Group* node = agxOSG::createVisual(containerAssembly, root);
    setRenderColor(node, agx::Vec4f(0.2f, 0.4f, 1.0f, 1.0f), 120.0f, 0.2f);

    agxSDK::Assembly* assembly = new agxSDK::Assembly();

    double scraperSize[3] = {0.5, 0.1, 0.2};
    agxCollide::Geometry* scraperGeometry = new agxCollide::Geometry(new agxCollide::Box(scraperSize[0], scraperSize[1], scraperSize[2]));
    scraperGeometry->addGroup("scraper");
    agx::RigidBody* scraper = new agx::RigidBody(scraperGeometry);
    assembly->add(scraper);

    setRenderColor(agxOSG::createVisual(scraper, root), agx::Vec4f(0.0f, 0.0f, 0.0f, 1.0f), 120.0f, 1.0f);


    double conveyorSize[3] = {4.0, 1.0, 0.1};
    agxCollide::Geometry* conveyor = new agxCollide::Geometry(new agxCollide::Box(conveyorSize[0], conveyorSize[1], conveyorSize[2]));
    conveyor->addGroup("conveyor");  //so we can disable collisions between the "scraper" and the "conveyor"
    conveyor->setPosition(0.0, 0.0, 0.0);
    conveyor->setSurfaceVelocity(agx::Vec3f(1.0, 0.0, 0.0));
    assembly->add(conveyor);

    agxCollide::Geometry* conveyor2 = new agxCollide::Geometry(new agxCollide::Box(conveyorSize[0]-scraperSize[0], conveyorSize[2], conveyorSize[2]*3.0));
    conveyor2->addGroup("conveyor");
    conveyor2->setPosition(-1.0*scraperSize[0], conveyorSize[1]-conveyorSize[2], conveyorSize[2]*4.0);
    assembly->add(conveyor2);

    agxCollide::Geometry* conveyor3 = new agxCollide::Geometry(new agxCollide::Box(conveyorSize[0]-scraperSize[0], conveyorSize[2], conveyorSize[2]*3.0));
    conveyor3->addGroup("conveyor");
    conveyor3->setPosition(-1.0*scraperSize[0], -1.0*conveyorSize[1] + conveyorSize[2], conveyorSize[2]*4.0);
    assembly->add(conveyor3);

    agxCollide::Geometry* conveyor4 = new agxCollide::Geometry(new agxCollide::Box(conveyorSize[2], conveyorSize[1], conveyorSize[2] * 4.0));
    conveyor4->addGroup("conveyor");
    conveyor4->setPosition(conveyorSize[0]+conveyorSize[2], 0, conveyorSize[2]*4.0);
    assembly->add(conveyor4);

    scraper->setPosition(conveyorSize[0]-scraperSize[0], 0, scraperSize[2]+conveyorSize[2]);
    assembly->setPosition(conveyorSize[0]/2.0, 0.0, 0.0);

    agx::Frame* f1 = new agx::Frame();
    f1->setLocalRotate(agx::EulerAngles(0, 3.14159265/2.0, 3.14159265/2.0));
    agx::Prismatic* prismatic = new agx::Prismatic(scraper, f1);
    prismatic->getRange1D()->setEnable(true);
    prismatic->setSolveType(agx::Constraint::DIRECT_AND_ITERATIVE);
    prismatic->getRange1D()->setRange(-1.0*conveyorSize[1]+scraperSize[1], conveyorSize[1]-scraperSize[1]);
    assembly->add(prismatic);

    createAlternatingSpeedController(sim, prismatic->getMotor1D(), 3.0, 1.0);

    sim->getSpace()->setEnablePair("conveyor", "scraper", false);

    node = agxOSG::createVisual(assembly, root); //reuses node...
    setRenderColor(node, agx::Vec4f(0.6f, 0.6f, 0.6f, 1.0f), 120.0f, 0.3f);
    sim->add(assembly);







    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Here is how to create and add a coarse graining listener to your simulation.
    //It takes as arguments the granular body system from agx, a grid, and the coarse graining width.
    //The DSCListener can be used as a faster alternative to the CGListener, but it does not define as many fields.
    //Also it works completely differently. It does not need the width parameter.
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    using FTYPE = double;

    Tensor<FTYPE, 3> min{-5.0, -1.5, 0.0};
    Tensor<FTYPE, 3> max{5.0, 1.5, 8.0};
    Tensor<size_t, 3> cells{40, 3, 25};
    Fern::Grid grid(min, max, cells);

    //CGListener<FTYPE>* cglistener = new CGListener<FTYPE>(granularBodySystem, grid, 0.5);
    DSCListener<FTYPE>* cglistener = new DSCListener<FTYPE>(granularBodySystem, grid);

    sim->add(cglistener);






    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Lastly we create a FieldPrinter, which is the class we defined at the start of this file.
    //It demonstrates how to extract data from the CGListener class.
    //There is no need to subclass the CGListener.
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    sim->add(new FieldPrinter(cglistener));




    return root;
}





///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//The rest of this file is just the usual agx stuff to properly start the script.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int main( int argc, char** argv )
{
    std::cout<<std::showpos<<std::fixed<<std::setprecision(3); //nicer output, but is ignored by glm::to_string(...)

    agx::AutoInit agxInit;
    agxOSG::ExampleApplicationRef application = new agxOSG::ExampleApplication;

    application->addScene( buildScene1, '1' );

    application->init(argc, argv);
    return application->run();
}
