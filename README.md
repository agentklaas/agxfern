Agxfern
===

Features
---
This library provides an integration of the fern discretization library with agx.
It provides StepEventListeners that calculate a number of fields from any agx::ParticleSystem.

While it is written in and mainly for C++, it also provides some rudimentary python bindings to be of use to a wider audience.




Dependencies
---
You will need a C++17 capable compiler and cmake to build the project.
Of course, you will also need to have agx and its dependencies installed on your system.  

Apart from these programs, this library depends on several other projects as git submodules.  
Most of them are libraries, which I originally developed for just this project, but some of them have transcended that use since.  
**Stine** defines the Tensor struct, which is widely used almost everywhere.  
**Comet** defines the Tuple struct, which is used as a better alternative to the std::tuple class.  
**Fern** does all the heavy lifting. Agxfern just wraps around fern to work with agx and to compute more useful secondary fields.  
**pybind11** is used to create the python bindings. It is the only submodule developed by a third party. It itself requires the python3 development libraries. ("python3-dev" in Ubuntu)

I have only tested building this project under Ubuntu 20.04.
Your experience on other system may vary.


How to Build
---
If you just cloned this project and want to build it for the first time, you will not have the submodules.
To fetch them type:

```
git submodule update --init --recursive
```


Or if you already have an outdated version of the submodules and want to update them:

```
git submodule update --remote --recursive
```


Once you have the submodules, you should be able to build the project with cmake.
(Your environment variables must be set appropriately to use agx. 
Usually you would run "source /opt/Algoryx/AgX-2.30.1.0/setup_env.bash", or something of that sort, but I had trouble with this method in the past. I blame agx.)

I recommend building in a subdirectory like so:

```
mkdir build
cd build
cmake ..
make
```


This will build two targets:  
One is compiled from example.cpp, which is the C++ example.  
The other is the python module, compiled from agxfernpy.cpp, which contains the discretization listeners.



Running the Examples
---
The library comes with two implementations (one in C++ and one in python) of an example.
After building as described above, you should be able to run them with the following commands:  

```
./example
python3 ../example.py
```




Files
===

example.cpp
---
The most important file for the end user.
It provides commentary on how to actually use the library, complete with a description for the entire agx scene.
The scene is pretty much a direct transcription of the agx provided "tutorial_granularBodies.py".


example.py
---
The same but for python end users.
The scene description is mostly taken from the "tutorial_granularBodies.py" file, that agx provides.


agxfernpy.cpp
---
Contains the binding code for the python module.


agxfernpy.h
---
Contains wrapper structs for the important C++ structs and some conversion functions.


Particle.h
---
It's a pain to deal with the variable agx data types, so this struct copies, formats and cast the data to use **Stine** tensors of a fixed precision type.


Contact.h
---
The same as Particle.h but for contacts between particles.


DSCListener.h
---
Contains the class derived from StepEventListener, that uses **fern**'s classical discretization approach to calculate a few fields at a fast speed.


CGListener.h
---
Contains the class derived from StepEventListener, that uses **fern**'s coarse graining.
It calculates a larger number of physical fields than the DSCListener.
However, it is much slower. This can be largely attributed to the exponential function in the gaussian, which is rather slow to compute.


LICENSE
---
Contains the License.


README.md
---
It's this file.
