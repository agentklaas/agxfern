import agx
import agxCollide
import agxOSG
import agxSDK
import agxIO
import agxPython
import math
import sys

import numpy as np
import os

import agxfernpy as afp


###################################################################################################################
#An exemplary class that shows how to access the coarse graining data in python.
#The coarse grainer itself is defined in the agxfernpy module, which is compiled from C++.
#You will find how to add the coarse graining listener itself to your simulation further down.
###################################################################################################################

class FieldPrinter(agxSDK.StepEventListener):
    def __init__(self, cglistener):
        self.cglistener = cglistener
        self.cells = self.cglistener.get_grid().get_cells()
        super().__init__()


    def post(self, time):
        total_mass = 0.0
        total_momentum = [0.0]*3

        mass = self.cglistener.get_mass()
        momentum = self.cglistener.get_momentum()
        for i in range(self.cells[0]):
            for j in range(self.cells[1]):
                for k in range(self.cells[2]):
                    total_mass += mass[i,j,k]
                    for l in range(3):
                        total_momentum[l] += momentum[i,j,k,l]


        total_mass = total_mass*self.cglistener.get_grid().cell_volume()
        total_momentum = [comp*self.cglistener.get_grid().cell_volume() for comp in total_momentum]
        print(total_mass, total_momentum)





###################################################################################################################
#Here starts the scene description etc.
#If you are familiar with agx, then you can ignore this part.
#It is adopted from the tutorial_granularBodies.py, that ships with agx.
###################################################################################################################

class MotorStepListener(agxSDK.StepEventListener):
    def __init__(self, motor, interval, speed):
        self.motor = motor
        self.interval = interval
        self.last = 0
        self.speed = speed
        super().__init__(agxSDK.StepEventListener.PRE_STEP)

    def pre(self, time):
        if (time - self.last >= self.interval):
            self.last = time
            self.speed = -self.speed
            self.motor.setSpeed(self.speed)



def createAlternatingSpeedController(sim, motor, speed, interval):
    listener = MotorStepListener(motor, interval, speed)
    motor.setEnable(True)
    motor.setSpeed(speed)
    sim.add(listener)


def setRenderColor(node, d):
    agxOSG.setDiffuseColor(node, d["diffuse"])
    agxOSG.setAmbientColor(node, agx.Vec4f(1.0, 1.0, 1.0, 0))
    agxOSG.setSpecularColor(node, agx.Vec4f(1.0, 1.0, 1.0, 1.0))
    agxOSG.setShininess(node, d["shininess"])
    agxOSG.setAlpha(node, d["alpha"])


def buildScene1(sim, app, root):
    agx.setNumThreads(0)
    n = int(agx.getNumThreads() / 2)
    agx.setNumThreads(n)

    sim.getDynamicsSystem().getSolver().setUseParallelPgs(True)
    sim.getDynamicsSystem().getSolver().setUse32bitGranularBodySolver(True)

    floor_geometry = agxCollide.Geometry(agxCollide.Box(10, 10, 0.2))
    floor = agx.RigidBody(floor_geometry)
    floor.setPosition(0, 0, -0.2)
    floor.setMotionControl(agx.RigidBody.STATIC)
    sim.add(floor)
    agxOSG.createVisual(floor, root)

    granularBodySystem = agx.GranularBodySystem()
    sim.add(granularBodySystem)


    pelletmat = agx.Material("pellet")
    floormat = agx.Material("floor")

    pelletmat.getBulkMaterial().setDensity(2500)
    c = agx.ContactMaterial(pelletmat, floormat)
    c2 = agx.ContactMaterial(pelletmat, pelletmat)
    c.setYoungsModulus(1.1e+11)
    c.setFrictionCoefficient(0.5)
    c.setRollingResistanceCoefficient(0.3)

    c2.setYoungsModulus(1.1e+11)
    c2.setFrictionCoefficient(0.5)
    c2.setRollingResistanceCoefficient(0.3)

    sim.add(c)
    sim.add(c2)

    granularBodySystem.setParticleRadius(0.08)
    granularBodySystem.setMaterial(pelletmat)

    floor_geometry.setMaterial(floormat)


    containerBound = agx.Bound3(agx.Vec3(-1.0, -1.0, 3), agx.Vec3(1.0, 1.0, 6))
    granularBodySystem.spawnParticlesInBound(containerBound, granularBodySystem.getParticleRadius(), agx.Vec3(2.0 * granularBodySystem.getParticleRadius()), 0.3 * granularBodySystem.getParticleRadius())
    particles = granularBodySystem.getParticles()

    for i in range(0, len(particles)):
        particles[i].setMaterial(pelletmat)


    print("Particle system now has {} particles".format(granularBodySystem.getNumParticles()))
    app.getSceneDecorator().setText(1, "#particles: {}".format(granularBodySystem.getNumParticles()))

    agxOSG.createVisual(granularBodySystem, root)


    size = agx.Vec3(2 * 0.8, 0.1, 3 * 0.8)
    pos = agx.Vec3(-1+size.x()*0.6, -1.0-size.y()*1.9, 4-size.z()*0.2)
    rotation = 20

    containerAssembly = agxSDK.Assembly()

    w1 = agxCollide.Geometry(agxCollide.Box(size))
    w1.setPosition(pos.x(), pos.y(), pos.z())
    w1.setRotation(agx.EulerAngles(math.radians(rotation), 0, 0))
    containerAssembly.add(w1)

    w2 = agxCollide.Geometry(agxCollide.Box(size))
    w2.setPosition(-pos.y(), pos.x(), pos.z())
    w2.setRotation(agx.EulerAngles(math.radians(rotation), 0, math.radians(90)))
    containerAssembly.add(w2)

    w3 = agxCollide.Geometry(agxCollide.Box(size))
    w3.setPosition(pos.y(), -pos.x(), pos.z())
    w3.setRotation(agx.EulerAngles(math.radians(rotation), 0, math.radians(-90)))
    containerAssembly.add(w3)

    w4 = agxCollide.Geometry(agxCollide.Box(size))
    w4.setPosition(pos.x(), -pos.y(), pos.z())
    w4.setRotation(agx.EulerAngles(math.radians(rotation), 0, math.radians(180)))
    containerAssembly.add(w4)

    sim.add(containerAssembly)

    node = agxOSG.createVisual(containerAssembly, root)
    setRenderColor(node, {"diffuse": agx.Vec4f(0.2, 0.4, 1.0, 1.0), "shininess": 120, "alpha": 0.2})


    assembly = agxSDK.Assembly()

    scraperSize = [0.5, 0.1, 0.2]
    scraperGeometry = agxCollide.Geometry(agxCollide.Box(scraperSize[0], scraperSize[1], scraperSize[2]))
    scraperGeometry.addGroup("scraper")
    scraper = agx.RigidBody(scraperGeometry)
    assembly.add(scraper)

    setRenderColor(agxOSG.createVisual(scraper, root), {"diffuse": agx.Vec4f(0.0, 0.0, 0.0, 1.0), "shininess": 120, "alpha": 1})


    conveyorSize = [4, 1, 0.1]
    conveyor = agxCollide.Geometry(agxCollide.Box(conveyorSize[0], conveyorSize[1], conveyorSize[2]))
    conveyor.addGroup("conveyor") #so we can disable collisions between the "scraper" and the "conveyor"
    conveyor.setPosition(0, 0, 0)
    conveyor.setSurfaceVelocity(agx.Vec3f(1, 0, 0))
    assembly.add(conveyor)

    conveyor2 = agxCollide.Geometry(agxCollide.Box(conveyorSize[0] - scraperSize[0], conveyorSize[2], conveyorSize[2] * 3))
    conveyor2.addGroup("conveyor")
    conveyor2.setPosition(-scraperSize[0], conveyorSize[1] - conveyorSize[2], conveyorSize[2] * 4)
    assembly.add(conveyor2)

    conveyor3 = agxCollide.Geometry(agxCollide.Box(conveyorSize[0] - scraperSize[0], conveyorSize[2], conveyorSize[2] * 3))
    conveyor3.addGroup("conveyor")
    conveyor3.setPosition(-scraperSize[0], -conveyorSize[1] + conveyorSize[2], conveyorSize[2] * 4)
    assembly.add(conveyor3)

    conveyor4 = agxCollide.Geometry(agxCollide.Box(conveyorSize[2], conveyorSize[1], conveyorSize[2] * 4))
    conveyor4.addGroup("conveyor")
    conveyor4.setPosition(conveyorSize[0] + conveyorSize[2], 0, conveyorSize[2] * 4)
    assembly.add(conveyor4)

    scraper.setPosition(conveyorSize[0] - scraperSize[0], 0, scraperSize[2] + conveyorSize[2])
    assembly.setPosition(conveyorSize[0] / 2, 0, 0)


    f1 = agx.Frame()
    f1.setLocalRotate(agx.EulerAngles(0, math.radians(90), math.radians(90)))
    prismatic = agx.Prismatic(scraper, f1)
    prismatic.getRange1D().setEnable(True)
    prismatic.setSolveType(agx.Constraint.DIRECT_AND_ITERATIVE)
    prismatic.getRange1D().setRange(-conveyorSize[1] + scraperSize[1], conveyorSize[1] - scraperSize[1])
    assembly.add(prismatic)

    createAlternatingSpeedController(sim, prismatic.getMotor1D(), 3.0, 1.0)


    sim.getSpace().setEnablePair("conveyor", "scraper", False)


    node = agxOSG.createVisual(assembly, root)
    setRenderColor(node, {"diffuse": agx.Vec4f(0.6, 0.6, 0.6, 1), "shininess": 120, "alpha": 0.3})
    sim.add(assembly)




###################################################################################################################
#Here is how to add the coarse graining listener to your simulation.
#It takes as arguments the granular body system from agx, a grid, and the coarse graining width.
#The grid class holds the boundaries and the number of cells per axis.
#It has some utility functions as well, which you'll find in the agxfernpy.h file.
#The agxfernpy module is built using pybind11, while agx itself uses the much more complicated SWIG.
#This means you will have to to do some pointer magic as shown below.
#You pass the granularBodySystem with ".this" to access the underlying data of the SWIG object.
#Then you add the listener to the simulation with a member function of the listener.
#
#The DSCListener can be used as a faster alternative to the CGListener, but it does not define as many fields.
#Also it works completely differently.
#
#All the accessors and other member functions of the listeners can be found in the agxfernpy.h file as well.
###################################################################################################################

    mins = [-5.0, -1.5, 0.0]
    maxs = [5.0, 1.5, 8.0]
    cells = [40, 3, 25]
    grid = afp.Grid(mins, maxs, cells)

    cglistener = afp.DSCListener(granularBodySystem.this, grid)
    #cglistener = afp.CGListener(granularBodySystem.this, grid, 0.5)

    cglistener.add_to_sim(sim.this)




###################################################################################################################
#Lastly we create a FieldPrinter, which is the class we defined at the start of this file.
#It demonstrates how to extract data from the CGListener class.
###################################################################################################################

    sim.add(FieldPrinter(cglistener))





###################################################################################################################
#The rest of this file is just the usual, convoluted agx stuff to start the script.
###################################################################################################################

def buildScene():
    app = agxPython.getContext().environment.getApplication()
    sim = agxPython.getContext().environment.getSimulation()
    root = agxPython.getContext().environment.getSceneRoot()

    return buildScene1(sim, app, root)


def main(args):
    app = agxOSG.ExampleApplication()
    argParser = agxIO.ArgumentParser([sys.executable] + args)
    app.addScene(argParser.getArgumentName(1), "buildScene", ord('1'), True)

    if app.init(argParser):
        app.run()
    else:
        print("An error occurred while initializing ExampleApplication.")


if agxPython.getContext() is None:
    init = agx.AutoInit()
    main(sys.argv)
